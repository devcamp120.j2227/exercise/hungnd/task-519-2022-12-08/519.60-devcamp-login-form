import "bootstrap/dist/css/bootstrap.min.css";
import LoginForm from "./components/loginFormBody";
import {Container} from "reactstrap";

import './App.css';

function App() {
  return (
    <Container style={{ width:"600px",marginTop:"50px", backgroundColor:"rgb(36, 51, 60)",padding:"20px"}}>
      <LoginForm/>
    </Container>
  );
}

export default App;
