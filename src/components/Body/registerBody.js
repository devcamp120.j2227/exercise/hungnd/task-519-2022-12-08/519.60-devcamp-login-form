import { Input, Row, Label, FormGroup, Button, Col } from "reactstrap";
import { useState } from "react"
function Register() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailSignUp, setEmailSingUp] = useState("");
    const [passwordSignUp, setPasswordSignUp] = useState("");

    const changeFirstNameHandler = (event) => {
        setFirstName(event.target.value);
    };

    const changeLastNameHandler = (event) => {
        setLastName(event.target.value);
    };

    const changeSignUpEmailHandler = (event) => {
        setEmailSingUp(event.target.value);
    };

    const changeSignUpPasswordHandler = (event) => {
        setPasswordSignUp(event.target.value);
    };
    const buttonSignUp = () => {
        if (firstName === "") {
            alert("First Name is not valid");
            return false;
        }
        if (lastName === "") {
            alert("Last Name is not valid");
            return false;
        }
        if (emailSignUp === "") {
            alert("Email is not valid");
            return false;
        }
        if (passwordSignUp === "") {
            alert("Password is not valid");
            return false;
        }
        console.log("Button Signup Clicked!");
        console.log("First Name: ", firstName);
        console.log("Last Name: ", lastName);
        console.log("Email: ", emailSignUp);
        console.log("Password: ", passwordSignUp);
        return true;
    };

    return (
        <>
            <div className="div-signup">
                <Row className="text-center mt-5 mb-3">
                    <h2 style={{ color: "white" }}>
                        Sign Up For Free
                    </h2>
                </Row>
                <Row>
                    <Col sm={6}>
                        <FormGroup floating>
                            <Input

                                id="firstname"
                                placeholder="name"
                                type="name"
                                style={{backgroundColor: "#24333C", color: "#fff"}}
                                onChange={changeFirstNameHandler}
                                value={firstName}
                            />
                            <Label for="firstname" style={{ marginLeft: "0px" }}>
                                First Name <span>*</span>
                            </Label>
                        </FormGroup>
                    </Col>
                    <Col sm={6}>
                        <FormGroup floating>
                            <Input
                                id="lastname"
                                placeholder="name"
                                type="name"
                                style={{backgroundColor: "#24333C", color: "#fff"}}
                                onChange={changeLastNameHandler}
                                value={lastName}
                            />
                            <Label for="lastname" style={{ marginLeft: "0px" }}>
                                Last Name <span>*</span>
                            </Label>
                        </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <FormGroup floating>
                        <Input
                            id="Email"
                            placeholder="Email"
                            type="email"
                            style={{backgroundColor: "#24333C", color: "#fff"}}
                            onChange={changeSignUpEmailHandler}
                            value={emailSignUp}
                        />
                        <Label for="Email">
                            Email Address <span>*</span>
                        </Label>
                    </FormGroup>
                </Row>
                <Row>
                    <FormGroup floating>
                        <Input
                            id="Password"
                            placeholder="password"
                            type="password"
                            style={{backgroundColor: "#24333C", color: "#fff"}}
                            onChange={changeSignUpPasswordHandler}
                            value={passwordSignUp}
                        />
                        <Label for="Password">
                            Set Password <span>*</span>
                        </Label>
                    </FormGroup>
                </Row>
                <Row className="mt-3">
                    <div >
                        <Button className="form-control"><h3 className="mb-0" onClick={buttonSignUp}>GET STARTED</h3></Button>
                    </div>
                </Row>
            </div>
        </>
    )
}
export default Register;