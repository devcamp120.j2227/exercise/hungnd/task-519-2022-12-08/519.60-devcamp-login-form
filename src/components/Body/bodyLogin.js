import { useState } from "react";
import { Input, Row, Label, FormGroup, Button } from "reactstrap";

const LoginBody = () => {
    const [email, setEmailChange] = useState("");

    const [password, setPasswordChange] = useState("");

    const onChangeEmail = (event) => {
        setEmailChange(event.target.value);
    }
    const onChangePassword = (event) => {
        setPasswordChange(event.target.value)
    }
    const onBtnLogin = () => {
        console.log("Button Login Clicked!")
        if ((email !== "") && (password !== "")) {
            console.log("Email: " + email, "Password: " + password);
            return true;
        }
        else {
            alert("Check email and password again");
            return false;
        }
    }

    return (
        <>
            <div className="div-login">
                <Row className="text-center mt-5 mb-3">
                    <h2 style={{ color: "white" }}>
                        Welcome Back!
                    </h2>
                </Row>
                <Row>
                    <FormGroup floating>
                        <Input
                            id="Email"
                            placeholder="Email"
                            type="email"
                            style={{backgroundColor: "#24333C", color: "#fff"}}
                            onChange={onChangeEmail}
                            value={email}
                        />
                        <Label for="Email">
                            Email Address <span>*</span>
                        </Label>
                    </FormGroup>
                </Row>
                <Row>
                    <FormGroup floating>
                        <Input
                            id="Password"
                            placeholder="password"
                            type="password"
                            style={{backgroundColor: "#24333C", color: "#fff"}}
                            onChange={onChangePassword}
                            autoComplete="current-password"
                            value={password}
                        />
                        <Label for="Password">
                            Password <span>*</span>
                        </Label>
                    </FormGroup>
                </Row>
                <Row >
                    <div className="d-flex justify-content-end">
                        <a href="#" style={{textDecoration: "none" }}>Forgot Password?</a>
                    </div>
                </Row>
                <Row className="mt-3">
                    <div >
                        <Button className="form-control"><h3 className="mb-0" onClick={onBtnLogin}>LOG IN</h3></Button>
                    </div>
                </Row>
            </div>
        </>
    )
}
export default LoginBody;