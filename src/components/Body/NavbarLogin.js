import { useState } from "react";
import { Row, Button, ButtonGroup } from "reactstrap";
import LoginBody from "./bodyLogin";
import Register from "./registerBody";

const NavbarLogin = () => {
    const [ActiveStatusLogin, setActiveStatusLog] = useState(true);
    const [ActiveStatusRegis, setActiveStatusReg] = useState(false);
    const [ColorLog, setColorLog] = useState("#1AB188")
    const [ColorReg, setColorReg] = useState("#435359")
    const LoginButtonClick = () => {
        setActiveStatusLog(true);
        setActiveStatusReg(false);
        setColorLog("#1AB188");
        setColorReg("#435359")

    }
    const RegisterButtonClick = () => {
        setActiveStatusReg(true);
        setActiveStatusLog(false);
        setColorReg("#1AB188");
        setColorLog("#435359")

    }
    return (
        <>
            <Row>
                <ButtonGroup>
                    <Button id="btn-login" style={{ backgroundColor: ColorLog }} onClick={LoginButtonClick} active={ActiveStatusLogin}>
                        Log In
                    </Button>
                    <Button id="btn-register" style={{ backgroundColor: ColorReg }} onClick={RegisterButtonClick} active={ActiveStatusRegis}>
                        Sign Up
                    </Button>
                </ButtonGroup>
            </Row>
            {ActiveStatusLogin ? <LoginBody /> : <Register />}
        </>
    )
}
export default NavbarLogin;